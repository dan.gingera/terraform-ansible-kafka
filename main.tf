terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70.1"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.aws_region
}

resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = true

  tags = {
    Name = "main"
  }
}

resource "aws_key_pair" "dgingera" {
  key_name          = "dgingera"
  public_key        = file(var.path_to_public_key)
}


resource "local_file" "inventory" {
  depends_on = [ aws_nat_gateway.nat_gateway ]
  filename = "hosts"
  content = <<EOF
[all:vars]
ansible_ssh_common_args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ProxyCommand="ssh -W %h:%p -q ubuntu@${aws_eip.jumpbox.public_dns}"'

[kafka]
${aws_instance.kafka-terraform[0].private_ip} node_name=kafka-1 broker_id=0
${aws_instance.kafka-terraform[1].private_ip} node_name=kafka-2 broker_id=1
${aws_instance.kafka-terraform[2].private_ip} node_name=kafka-3 broker_id=2

[zookeeper]
${aws_instance.zookeeper-terraform[0].private_ip} node_name=zookeeper-1 private_ip=${aws_instance.zookeeper-terraform[0].private_ip}
${aws_instance.zookeeper-terraform[1].private_ip} node_name=zookeeper-2 private_ip=${aws_instance.zookeeper-terraform[1].private_ip}
${aws_instance.zookeeper-terraform[2].private_ip} node_name=zookeeper-3 private_ip=${aws_instance.zookeeper-terraform[2].private_ip}

EOF
}

resource "time_sleep" "wait_30_seconds" {
  depends_on = [ local_file.inventory ]
  create_duration = "30s"
}

resource "null_resource" "zookeeper" {
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i hosts ansible/zookeeper/zookeeper-setup.yml"
  }
    depends_on = [ time_sleep.wait_30_seconds ]
}

resource "null_resource" "kafka" {
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i hosts ansible/kafka/kafka-setup.yml"
  }
    depends_on = [ null_resource.zookeeper ]
}