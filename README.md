<!-- ABOUT THE PROJECT -->
## About The Project

I created this project as a reference clustered deployment of Kafka + Zookeeper + Jumpbox. Terraform provisions the environment, Ansible configures the software and clustering.


This project will deploy the following in AWS:
- 3 x Kafka Nodes
- 3 x Zookeeper Nodes
- 1 x Jumpbox (primarily for Ansible so fewer Elastic IPs are necessary)

The project also handles all underlay infrastructure, such as:

- VPC
- Routes
- Security Groups
- Public and Private Subnets
- Internet and NAT Gateways
- Elastic IPs (used for NAT gateway and Jumpbox)

<p align="right">(<a href="#top">back to top</a>)</p>

![](images/terraform-ansible-kafka.png)

### Built With

* [Terraform](https://terraform.io/)
* [Ansible](https://ansible.com/)

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

Use the `vars.tf` file to specify the AMIs you want to use as well as the instance counts for the Kafka and Zookeeper nodes. Note: The project is not
completely dynamic and is only working with 3 x 3 discreet nodes for now. 

Create an SSH-key pair and drop it in the project root. The public key is installed on all nodes, and the private key is copied to the Jumpbox. This is necessary so Ansible can tunnel
through the Jumpbox and configure the nodes in the private subnet.

Once Terraform has done its work, it will automatically kick off Ansible

### Prerequisites

* Ansible ~> 2.9
* Terraform ~> 1.1.7 (probably works with different versions, but untested)


<!-- USAGE EXAMPLES -->
## Usage

`terraform init`

`terraform apply`

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [ ] Add support for any node count
- [ ] Add ssh-key auto creation

<p align="right">(<a href="#top">back to top</a>)</p>

