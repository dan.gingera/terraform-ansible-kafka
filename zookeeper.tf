resource "aws_instance" "zookeeper-terraform" {
  ami                    = var.zookeeper_ami
  instance_type          = var.zookeeper_instance_type
  count                  = var.zookeeper_instance_count
  key_name               = aws_key_pair.dgingera.key_name
  vpc_security_group_ids = [aws_security_group.zookeeper.id]
  subnet_id              = aws_subnet.private.id
  tags = {
    Name = "zookeeper-${count.index + 1}"
  }
}


