# Kafka Security Groups
resource "aws_security_group" "kafka" {
  name        = "Kafka Rules"
  description = "Managed by Terraform - Do not Touch!"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "Kafka Traffic"
    from_port        = 9092
    to_port          = 9092
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Kafka"
  }
}

# Zookeeper Security Groups
resource "aws_security_group" "zookeeper" {
  name        = "Zookeeper Rules"
  description = "Managed by Terraform - Do not Touch!"
  vpc_id      =  aws_vpc.main.id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "Zookeeper Cluster Traffic"
    from_port        = 2888
    to_port          = 2888
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "Zookeeper Cluster Traffic"
    from_port        = 3888
    to_port          = 3888
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "Zookeeper Client Traffic"
    from_port        = 2181
    to_port          = 2181
    protocol         = "tcp"
    security_groups  = [aws_security_group.kafka.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Zookeeper"
  }
}

