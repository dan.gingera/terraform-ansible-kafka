resource "aws_instance" "jumpbox-terraform" {
  ami                    = var.zookeeper_ami
  instance_type          = var.zookeeper_instance_type
  key_name               = aws_key_pair.dgingera.key_name
  vpc_security_group_ids = [aws_security_group.zookeeper.id]
  subnet_id              = aws_subnet.public.id
  tags = {
    Name = "jumpbox-1"
  }
}

resource "aws_eip" "jumpbox" {
  instance               = aws_instance.jumpbox-terraform.id
  vpc                    = true
  tags = {
    Name = "jumpbox-eip-1"
  }
  depends_on = [aws_internet_gateway.gw]

    provisioner "file" {
      source      = "id_rsa"
      destination = "/home/ubuntu/.ssh/id_rsa"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("id_rsa")
      host        = self.public_dns
    }
  }
}
