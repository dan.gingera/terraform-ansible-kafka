variable "aws_region" { default = "ca-central-1" }
variable "path_to_public_key" { default = "id_rsa.pub" }
variable "kafka_instance_type" { default = "t2.micro"}
variable "kafka_instance_count" { default = 3 }
variable "kafka_ami" { default = "ami-0aee2d0182c9054ac" }
variable "zookeeper_instance_type" { default = "t2.micro"}
variable "zookeeper_instance_count" { default = 3 }
variable "zookeeper_ami" { default = "ami-0aee2d0182c9054ac" }