resource "aws_instance" "kafka-terraform" {
  ami                    = var.kafka_ami
  instance_type          = var.kafka_instance_type
  count                  = var.kafka_instance_count
  key_name               = aws_key_pair.dgingera.key_name
  vpc_security_group_ids = [aws_security_group.kafka.id]
  subnet_id              = aws_subnet.private.id
  tags = {
    Name = "kafka-${count.index + 1}"
  }
}


